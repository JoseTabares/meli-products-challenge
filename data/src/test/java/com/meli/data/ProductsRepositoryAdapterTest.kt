package com.meli.data


import com.meli.models.SearchProductsResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProductsRepositoryAdapterTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var productsApiSource: ProductsApiSource
    private lateinit var repository: ProductsRepositoryAdapter

    @Before
    fun setUp() {
        repository = ProductsRepositoryAdapter(productsApiSource)
    }

    @Test
    fun getProductsByShouldReturnDataFromApiSource() {
        testCoroutineRule.runBlockingTest {
            val search = "search"
            val expected = SearchProductsResult()
            doReturn(expected).`when`(productsApiSource).getProductsBy(search)
            val result = repository.getProductsBy(search)
            verify(productsApiSource).getProductsBy(search)
            Assert.assertEquals(result, expected)
        }

    }
}