package com.meli.data

import com.meli.models.SearchProductsResult

interface ProductsApiSource {
    suspend fun getProductsBy(search: String): SearchProductsResult
}