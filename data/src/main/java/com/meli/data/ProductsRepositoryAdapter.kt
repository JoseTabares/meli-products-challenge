package com.meli.data

import com.meli.domain.ProductsRepository
import com.meli.models.SearchProductsResult
import javax.inject.Inject

class ProductsRepositoryAdapter @Inject constructor(private val productsApiSource: ProductsApiSource) :
    ProductsRepository {

    override suspend fun getProductsBy(search: String): SearchProductsResult {
        return productsApiSource.getProductsBy(search)
    }
}