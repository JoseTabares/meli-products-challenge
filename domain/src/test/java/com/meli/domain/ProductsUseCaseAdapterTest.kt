package com.meli.domain

import com.meli.models.SearchProductsResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProductsUseCaseAdapterTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var productsRepository: ProductsRepository
    private lateinit var useCase: ProductsUseCaseAdapter

    @Before
    fun setUp() {
        useCase = ProductsUseCaseAdapter(productsRepository)
    }

    @Test
    fun getProductsByShouldReturnDataFromRepository() {
        testCoroutineRule.runBlockingTest {
            val search = "search"
            val expected = SearchProductsResult()
            doReturn(expected).`when`(productsRepository).getProductsBy(search)
            val result = useCase.getProductsBy(search)
            verify(productsRepository).getProductsBy(search)
            Assert.assertEquals(result, expected)
        }

    }
}