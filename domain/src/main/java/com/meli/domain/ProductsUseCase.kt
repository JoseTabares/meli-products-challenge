package com.meli.domain

import com.meli.models.SearchProductsResult

interface ProductsUseCase {
    @Throws(Exception::class)
    suspend fun getProductsBy(search: String): SearchProductsResult


}