package com.meli.domain

import com.meli.models.SearchProductsResult

interface ProductsRepository {
    suspend fun getProductsBy(search: String): SearchProductsResult
}