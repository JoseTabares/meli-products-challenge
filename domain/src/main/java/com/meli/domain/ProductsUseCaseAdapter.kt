package com.meli.domain

import com.meli.models.SearchProductsResult
import javax.inject.Inject


class ProductsUseCaseAdapter @Inject constructor(private val productsRepository: ProductsRepository) :
    ProductsUseCase {

    override suspend fun getProductsBy(search: String): SearchProductsResult {
        return productsRepository.getProductsBy(search)
    }


}