package com.meli.models

import java.io.Serializable


data class Sales(
    val completed: Int,
    val period: String
): Serializable