package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SearchProductsResult(
    @SerializedName("available_filters")
    var availableFilters: List<AvailableFilter> = emptyList(),
    @SerializedName("available_sorts")
    var availableSorts: List<AvailableSort> = emptyList(),
    @SerializedName("country_default_time_zone")
    var countryDefaultTimeZone: String = "",
    var filters: List<Filter> = emptyList(),
    var paging: Paging? = null,
    var query: String = "",
    var results: List<Result> = emptyList(),
    @SerializedName("site_id")
    var siteId: String = "",
    var sort: Sort?= null
): Serializable