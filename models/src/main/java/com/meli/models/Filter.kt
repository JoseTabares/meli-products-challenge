package com.meli.models

import java.io.Serializable


data class Filter(
    val id: String,
    val name: String,
    val type: String
): Serializable