package com.meli.models

import java.io.Serializable


data class Transactions(
    val canceled: Int,
    val completed: Int,
    val period: String,
    val ratings: Ratings,
    val total: Int
): Serializable