package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AvailableFilter(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String
): Serializable