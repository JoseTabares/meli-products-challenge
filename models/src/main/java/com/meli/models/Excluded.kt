package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Excluded(
    @SerializedName("real_rate")
    val realRate: Double,
    @SerializedName("real_value")
    val realValue: Int
): Serializable