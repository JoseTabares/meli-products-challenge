package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Paging(
    val limit: Int,
    val offset: Int,
    @SerializedName("primary_results")
    val primaryResults: Int,
    val total: Int
): Serializable