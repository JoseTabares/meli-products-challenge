package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Metadata(
    @SerializedName("campaign_id")
    val campaignId: String,
    @SerializedName("promotion_id")
    val promotionId: String,
    @SerializedName("promotion_type")
    val promotionType: String
): Serializable