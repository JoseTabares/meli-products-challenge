package com.meli.models

import java.io.Serializable


data class AvailableSort(
    val id: String,
    val name: String
): Serializable