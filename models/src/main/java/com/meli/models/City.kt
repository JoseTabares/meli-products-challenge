package com.meli.models

import java.io.Serializable


data class City(
    val id: String,
    val name: String
): Serializable