package com.meli.models

import java.io.Serializable


data class Prices(
    val id: String,
    val presentation: Presentation,
    val prices: List<Price>
): Serializable