package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Price(
    val amount: Double,
    val conditions: Conditions,
    @SerializedName("currency_id")
    val currencyId: String,
    @SerializedName("exchange_rate_context")
    val exchangeRateContext: String,
    val id: String,
    @SerializedName("last_updated")
    val lastUpdated: String,
    val metadata: Metadata,
    @SerializedName("type")
    val type: String
): Serializable