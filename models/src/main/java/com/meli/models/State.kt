package com.meli.models

import java.io.Serializable


data class State(
    val id: String,
    val name: String
): Serializable