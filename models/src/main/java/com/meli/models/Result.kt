package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Result(
    @SerializedName("accepts_mercadopago")
    val acceptsMercadopago: Boolean,
    val address: Address,
    val attributes: List<Attribute>,
    @SerializedName("available_quantity")
    val availableQuantity: Int,
    @SerializedName("buying_mode")
    val buyingMode: String,
    @SerializedName("catalog_listing")
    val catalogListing: Boolean,
    @SerializedName("catalog_product_id")
    val catalogProductId: String,
    @SerializedName("category_id")
    val categoryId: String,
    val condition: String,
    @SerializedName("currency_id")
    val currencyId: String,
    @SerializedName("differential_pricing")
    val differentialPricing: DifferentialPricing,
    @SerializedName("domain_id")
    val domainId: String,
    val id: String,
    val installments: Installments,
    @SerializedName("listing_type_id")
    val listingTypeId: String,
    @SerializedName("order_backend")
    val orderBackend: Int,
    val permalink: String,
    val price: Double,
    val prices: Prices,
    @SerializedName("sale_price")
    val salePrice: Double?,
    val seller: Seller,
    @SerializedName("seller_address")
    val sellerAddress: SellerAddress,
    val shipping: Shipping,
    @SerializedName("site_id")
    val siteId: String,
    @SerializedName("sold_quantity")
    val soldQuantity: Int,
    @SerializedName("stop_time")
    val stopTime: String,
    val tags: List<String>,
    val thumbnail: String,
    @SerializedName("thumbnail_id")
    val thumbnailId: String,
    val title: String,
    @SerializedName("use_thumbnail_id")
    val useThumbnailId: Boolean
) : Serializable