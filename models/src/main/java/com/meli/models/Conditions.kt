package com.meli.models

import java.io.Serializable


data class Conditions(
    val eligible: Boolean
): Serializable