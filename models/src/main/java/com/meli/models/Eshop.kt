package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Eshop(
    @SerializedName("eshop_experience")
    val eshopExperience: Int,
    @SerializedName("eshop_id")
    val eshopId: Int,
    @SerializedName("eshop_logo_url")
    val eshopLogoUrl: String,
    @SerializedName("eshop_status_id")
    val eshopStatusId: Int,
    @SerializedName("nick_name")
    val nickName: String,
    val seller: Int,
    @SerializedName("site_id")
    val siteId: String
): Serializable