package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Presentation(
    @SerializedName("display_currency")
    val displayCurrency: String
): Serializable