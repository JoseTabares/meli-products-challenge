package com.meli.models

import java.io.Serializable


data class Sort(
    val id: String,
    val name: String
): Serializable