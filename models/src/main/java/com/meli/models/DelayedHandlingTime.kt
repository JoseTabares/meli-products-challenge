package com.meli.models

import java.io.Serializable


data class DelayedHandlingTime(
    val excluded: Excluded,
    val period: String,
    val rate: Double,
    val value: Int
): Serializable