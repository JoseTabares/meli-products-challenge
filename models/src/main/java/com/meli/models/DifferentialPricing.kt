package com.meli.models

import java.io.Serializable


data class DifferentialPricing(
    val id: Int
): Serializable