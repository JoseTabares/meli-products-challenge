package com.meli.models

import java.io.Serializable


data class Ratings(
    val negative: Double,
    val neutral: Double,
    val positive: Double
): Serializable