package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SellerReputation(
    @SerializedName("level_id")
    val levelId: String,
    val metrics: Metrics,
    @SerializedName("power_seller_status")
    val powerSellerStatus: String,
    @SerializedName("protection_end_date")
    val protectionEndDate: String,
    @SerializedName("real_level")
    val realLevel: String,
    val transactions: Transactions
): Serializable