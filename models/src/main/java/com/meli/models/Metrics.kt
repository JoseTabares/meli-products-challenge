package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Metrics(
    val cancellations: Cancellations,
    val claims: Claims,
    @SerializedName("delayed_handling_time")
    val delayedHandlingTime: DelayedHandlingTime,
    val sales: Sales
): Serializable