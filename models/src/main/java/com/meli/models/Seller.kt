package com.meli.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Seller(
    @SerializedName("car_dealer")
    val carDealer: Boolean,
    val eshop: Eshop,
    val id: Int,
    val permalink: String,
    @SerializedName("real_estate_agency")
    val realEstateAgency: Boolean,
    @SerializedName("registration_date")
    val registrationDate: String,
    @SerializedName("seller_reputation")
    val sellerReputation: SellerReputation,
    val tags: List<String>
): Serializable