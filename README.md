# MELI Products Challenge

Aplicación Móvil para buscar y ver el detalle de los productos de Mercado Libre.

# Arquitectura Implementada

## Clean Architecture

Se implementa este principio para desacoplar cada una de las capas de la aplicación utilizando un principio llamado arquitectura modular.

- models (Módulo Kotlin) - Contiene todas las entidades del proyecto.
- domain (Módulo Kotlin) - Contiene todos los casos de uso del proyecto con todas las reglas de negocio.
- data (Módulo Kotlin) - Contiene todos los repositorios del proyecto y es el que decide donde va a ir a consultar la información ya sea en una base de datos local o un servicio web.
- app (Módulo Kotlin Android) - Es el módulo principal de la aplicación donde tenemos toda la UI, acceso a base de datos, consumo de servicios, view models, configuración de inyección de dependencias y demás características asociadas a la tecnología (Android).

### Imagen Ilustrativa

![title](https://www.implementandodevops.com/assets/images/service/service-03.png)

## Inyección de Dependencias

Se crearon interfaces para definir comportamientos entre cada una de las capas y con la librería llamada Hilt(Dagger) se hizo toda la configuración para resolver las dependencias.

## MVVM

Es el patrón de arquitectura implementado en la aplicación para desacoplar al máximo la lógica de interfaz de usuario llamando a cada de las clases como ViewModels y es la capa encargada de comunicarse con los casos de uso o reglas de negocio.

## Integración Continua

Se creó un pipeline de integración continua para correr dos stage.

- build stage - Es el encargado de generar el artefacto de instalación (apk) y generar un reporte de los problemas de la aplicación (lint).
- test stage - Es el encargado de correr las pruebas unitarias de cada uno de los módulos y generar los reportes de la cobertura del código.