package com.meli.products_challenge.di

import com.meli.data.ProductsRepositoryAdapter
import com.meli.domain.ProductsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

// Modulo de Hilt para resolver las dependecias de la capa "DOMAIN"
@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoriesModule {

    @Binds
    abstract fun bindProductsRepository(
        productsRepository: ProductsRepositoryAdapter
    ): ProductsRepository
}