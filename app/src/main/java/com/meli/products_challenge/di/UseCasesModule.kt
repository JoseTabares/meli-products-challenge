package com.meli.products_challenge.di

import com.meli.domain.ProductsUseCase
import com.meli.domain.ProductsUseCaseAdapter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

// Modulo de Hilt para resolver las dependecias de los casos de uso
@Module
@InstallIn(SingletonComponent::class)
abstract class UseCasesModule {

    @Binds
    abstract fun bindProductsUseCase(
        productsUseCase: ProductsUseCaseAdapter
    ): ProductsUseCase
}