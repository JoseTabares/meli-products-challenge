package com.meli.products_challenge.di

import com.meli.data.ProductsApiSource
import com.meli.products_challenge.data_source.api_source.ProductsApiSourceAdapter
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

// Modulo de Hilt para resolver las dependecias de la capa "DATA"
@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourcesModule {

    @Binds
    abstract fun bindProductsApiSource(
        productsApiSource: ProductsApiSourceAdapter
    ): ProductsApiSource
}