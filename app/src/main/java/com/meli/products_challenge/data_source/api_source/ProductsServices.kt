package com.meli.products_challenge.data_source.api_source

import com.meli.models.SearchProductsResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface ProductsServices {
    @GET("sites/MLA/search")
    suspend fun getProductsBy(@Query("q") search: String): Response<SearchProductsResult>
}