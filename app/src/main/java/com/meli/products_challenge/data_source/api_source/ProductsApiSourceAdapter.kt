package com.meli.products_challenge.data_source.api_source

import com.meli.data.ProductsApiSource
import com.meli.models.SearchProductsResult
import com.meli.models.errors.NetworkException
import com.meli.products_challenge.utils.ExceptionFactory
import javax.inject.Inject


class ProductsApiSourceAdapter @Inject constructor(private val productsServices: ProductsServices) :
    ProductsApiSource {
    override suspend fun getProductsBy(search: String): SearchProductsResult {
        try {
            val response = productsServices.getProductsBy(search)
            if (response.isSuccessful) {
                return response.body()!!
            }
            throw NetworkException(response.code(), "http error!")
        } catch (e: Exception) {
            throw ExceptionFactory.resolveError(e)
        }
    }

}