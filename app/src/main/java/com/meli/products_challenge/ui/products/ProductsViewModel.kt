package com.meli.products_challenge.ui.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.meli.domain.ProductsUseCase
import com.meli.models.SearchProductsResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

// View model de ProductsActivity
@HiltViewModel
class ProductsViewModel @Inject constructor(private val productsUseCase: ProductsUseCase) :
    ViewModel() {

    private val _searchResult = MutableLiveData<SearchProductsResult>()
    val searchResult: LiveData<SearchProductsResult> = _searchResult
    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading
    private val _error = MutableLiveData<Exception>()
    val error: LiveData<Exception> = _error


    fun getProductsBy(search: String) {
        viewModelScope.launch {
            _loading.value = true
            try {
                val result = productsUseCase.getProductsBy(search)
                _searchResult.value = result
            } catch (e: Exception) {
                _error.value = e
            } finally {
                _loading.value = false
            }
        }
    }
}