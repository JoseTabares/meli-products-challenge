package com.meli.products_challenge.ui.products

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.meli.models.Paging
import com.meli.models.Result
import com.meli.models.errors.NetworkException
import com.meli.products_challenge.R
import com.meli.products_challenge.databinding.ActivityProductsBinding
import com.meli.products_challenge.ui.product_detail.ProductDetailActivity
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.*

// Acitivity para buscar y mostrar el resultado de los productos
@AndroidEntryPoint
class ProductsActivity : AppCompatActivity() {

    private val productsViewModel: ProductsViewModel by viewModels()
    private lateinit var binding: ActivityProductsBinding

    private lateinit var searchView: SearchView
    private var currentSearch = ""

    private lateinit var adapter: ProductsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        setupRecyclerView()
        observeLoading()
        observeResults()
        observeError()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.options_menu, menu)

        // Get the SearchView and set the searchable configuration
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = (menu?.findItem(R.id.menu_search)?.actionView as SearchView)
        setupSearchView(searchManager)

        return true
    }

    private fun setupRecyclerView() {
        adapter = ProductsAdapter { result -> onClickResult(result) }
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
        } else {
            binding.recyclerView.layoutManager = LinearLayoutManager(this)
        }
        binding.recyclerView.adapter = adapter
    }

    private fun onClickResult(result: Result) {
        throw NullPointerException("Paila")
//        searchView.clearFocus()
//        val intent = Intent(this, ProductDetailActivity::class.java)
//        intent.putExtra("product", result)
//        startActivity(intent)
    }

    private fun setupSearchView(searchManager: SearchManager) {
        searchView.apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnSearchClickListener {
                searchView.setQuery(currentSearch, false)
            }
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(qString: String): Boolean {
                    return false
                }

                override fun onQueryTextSubmit(qString: String): Boolean {
                    manageSearch(qString)
                    return false
                }
            })
        }
    }

    private fun observeLoading() {
        productsViewModel.loading.observe(this, {
            if (it) {
                binding.progressCircular.visibility = View.VISIBLE
                binding.llSearchResult.visibility = View.GONE
            } else {
                binding.progressCircular.visibility = View.GONE
            }
        })
    }

    private fun observeResults() {
        productsViewModel.searchResult.observe(this, {
            it?.let {
                validateResults(it.results)
                adapter.submitList(it.results)
                loadTotalResults(it.paging!!)

            }
        })
    }

    private fun validateResults(results: List<Result>) {
        if (results.isEmpty()) {
            binding.tvSearchDescription.text = getString(R.string.products_not_found)
            binding.llSearchResult.visibility = View.VISIBLE
        } else {
            binding.llSearchResult.visibility = View.GONE
        }
    }

    private fun loadTotalResults(paging: Paging) {
        val format = NumberFormat.getInstance(Locale("es", "CO"))
        val text = "${format.format(paging.total)} ${getString(R.string.results)}"
        binding.tvResultsCount.text = text
    }


    private fun manageSearch(qString: String) {
        currentSearch = qString.trim()
        productsViewModel.getProductsBy(currentSearch)
    }

    private fun observeError() {
        productsViewModel.error.observe(this, {
            val message = getErrorMessage(it)
            Toast.makeText(
                applicationContext,
                message,
                Toast.LENGTH_LONG
            ).show()
        })
    }

    private fun getErrorMessage(error: Exception?): String {
        if (error is NetworkException) {
            return getString(R.string.not_internet)
        }

        return getString(R.string.default_error)
    }

}

