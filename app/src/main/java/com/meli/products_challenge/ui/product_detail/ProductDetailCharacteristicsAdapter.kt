package com.meli.products_challenge.ui.product_detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.meli.models.Attribute
import com.meli.products_challenge.R

// Adapter para mostrar las caracteristicas de un producto
class ProductDetailCharacteristicsAdapter(private var items: List<Attribute>) :
    RecyclerView.Adapter<ProductDetailCharacteristicsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.tv_name)
        private val value: TextView = view.findViewById(R.id.tv_value)

        fun bind(attribute: Attribute) {
            name.text = attribute.name
            value.text = attribute.valueName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.product_characteristic_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val attribute = items[position]
        holder.bind(attribute)
    }

    override fun getItemCount(): Int = items.size

}