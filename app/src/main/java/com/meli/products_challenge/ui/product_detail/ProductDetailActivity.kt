package com.meli.products_challenge.ui.product_detail

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.meli.models.Installments
import com.meli.models.Result
import com.meli.products_challenge.R
import com.meli.products_challenge.databinding.ActivityProductDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.*

// Activity para mostrar el detalle de un producto
@AndroidEntryPoint
class ProductDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProductDetailBinding

    private val currencyFormat = NumberFormat.getCurrencyInstance(Locale("es", "AR"))
    private val numberFormat = NumberFormat.getInstance(Locale("es", "CO"))

    private lateinit var characteristicsAdapter: ProductDetailCharacteristicsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        currencyFormat.maximumFractionDigits = 0


        val product = intent.getSerializableExtra("product") as Result
        showProductData(product)

        characteristicsAdapter = ProductDetailCharacteristicsAdapter(product.attributes)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = characteristicsAdapter
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showProductData(product: Result) {
        binding.tvSoldCount.text = getSoldCountText(product)
        binding.tvTitle.text = product.title
        Glide.with(this).load(product.thumbnail).into(binding.ivImage)
        binding.tvPrice.text = currencyFormat.format(product.price)
        loadInstallments(product.installments)
        loadStock(product.availableQuantity)
        binding.freeShipping.visibility =
            if (product.shipping.freeShipping) View.VISIBLE else View.GONE
    }

    private fun loadStock(availableQuantity: Int) {
        binding.tvAvailableStock.visibility = if (availableQuantity > 0) View.VISIBLE else View.GONE
        val text = "${numberFormat.format(availableQuantity)} ${getString(R.string.availables)}"
        binding.tvAvailableStockCount.text = text
    }

    private fun loadInstallments(installments: Installments?) {
        if (installments != null) {
            val text = "en ${installments.quantity}x ${currencyFormat.format(installments.amount)}"
            binding.tvInstallments.text = text
            binding.tvInstallments.visibility = View.VISIBLE
        } else {
            binding.tvInstallments.text = ""
            binding.tvInstallments.visibility = View.GONE
        }
    }

    private fun getSoldCountText(product: Result): String {
        val condition = getCondition(product.condition)
        return "$condition ${numberFormat.format(product.soldQuantity)} ${getString(R.string.sold)}"
    }

    private fun getCondition(condition: String): String {
        var result = ""
        when (condition) {
            "new" -> {
                result = getString(R.string.new_label)
            }
            "used" -> {
                result = getString(R.string.used)
            }
        }

        return "$result | "
    }
}