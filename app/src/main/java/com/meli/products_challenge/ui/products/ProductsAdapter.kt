package com.meli.products_challenge.ui.products

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.meli.models.Installments
import com.meli.models.Result
import com.meli.products_challenge.R
import java.text.NumberFormat
import java.util.*

// Adaptador para generar los items de los productos de manera optima
class ProductsAdapter(private val onClick: (Result) -> Unit) :
    ListAdapter<Result, ProductsAdapter.ViewHolder>(ProductsDiffCallback) {
    private val currencyFormat: NumberFormat = NumberFormat.getCurrencyInstance(Locale("es", "AR"))

    init {
        currencyFormat.maximumFractionDigits = 0
    }

    class ViewHolder(
        view: View,
        private val currencyFormat: NumberFormat,
        val onClick: (Result) -> Unit
    ) :
        RecyclerView.ViewHolder(view) {

        private val image: ImageView = view.findViewById(R.id.iv_image)
        private val title: TextView = view.findViewById(R.id.tv_title)
        private val price: TextView = view.findViewById(R.id.tv_price)
        private val installments: TextView = view.findViewById(R.id.tv_installments)
        private val freeShipping: TextView = view.findViewById(R.id.free_shipping)
        private var currentResult: Result? = null

        init {
            view.setOnClickListener {
                currentResult?.let {
                    onClick(it)
                }

            }
        }

        fun bind(result: Result) {
            currentResult = result
            title.text = result.title
            price.text = currencyFormat.format(result.price)
            Glide.with(image).load(result.thumbnail).into(image)
            freeShipping.visibility = if (result.shipping.freeShipping) View.VISIBLE else View.GONE
            val installmentsText = getInstallments(result.installments)
            if (installmentsText == null) {
                installments.text = ""
                installments.visibility = View.GONE
            } else {
                installments.text = installmentsText
                installments.visibility = View.VISIBLE
            }

        }

        private fun getInstallments(installments: Installments?): String? {
            if (installments != null) {
                return "en ${installments.quantity}x ${currencyFormat.format(installments.amount)}"
            }
            return null
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.product_item, parent, false)

        return ViewHolder(view, currencyFormat, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = getItem(position)
        holder.bind(result)
    }

}

object ProductsDiffCallback : DiffUtil.ItemCallback<Result>() {
    override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
        return oldItem.id == newItem.id
    }
}