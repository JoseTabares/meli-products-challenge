package com.meli.products_challenge

import android.app.Application
import android.util.Log
import com.facebook.stetho.Stetho
import dagger.hilt.android.HiltAndroidApp

// Clase principal de la aplicación
@HiltAndroidApp
class MELIApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this) //Permite interceptar las peticiones http con chrome://inspect/

        // Manejo de errores en toda la aplicación
        // Dependiendo de la necesidad del proyecto se pueden implementar estrategias de reporte de errores en algun sitio o simplemente mostralas en consola
        // Tambien se puede usar la herrameinto de Firebase Crashlytics para todo el manejo de errores
        // https://firebase.google.com/products/crashlytics
        val oldHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { thread, throwable ->
            Log.e("MELIApplication", throwable.message, throwable)
            oldHandler?.uncaughtException(thread, throwable)
        }
    }
}