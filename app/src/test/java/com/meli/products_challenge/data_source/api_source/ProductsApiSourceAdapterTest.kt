package com.meli.products_challenge.data_source.api_source


import com.meli.models.SearchProductsResult
import com.meli.products_challenge.TestCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProductsApiSourceAdapterTest {


    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var services: ProductsServices
    private lateinit var apiSource: ProductsApiSourceAdapter

    @Before
    fun setUp() {
        apiSource = ProductsApiSourceAdapter(services)
    }

    @Test
    fun getProductsByShouldReturnDataFromServices() {
        testCoroutineRule.runBlockingTest {
            val search = "search"
            val body = SearchProductsResult()
            val expected = Response.success(body)
            doReturn(expected).`when`(services).getProductsBy(search)
            val result = apiSource.getProductsBy(search)
            verify(services).getProductsBy(search)
            Assert.assertEquals(result, body)
        }

    }
}