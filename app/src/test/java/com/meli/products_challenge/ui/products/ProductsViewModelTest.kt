package com.meli.products_challenge.ui.products

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.meli.domain.ProductsUseCase
import com.meli.models.SearchProductsResult
import com.meli.models.errors.NetworkException
import com.meli.products_challenge.TestCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProductsViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var productsUseCase: ProductsUseCase

    private lateinit var viewModel: ProductsViewModel

    @Before
    fun setup() {
        viewModel = ProductsViewModel(productsUseCase)
    }

    @Test
    fun getProductsBySearchShouldReturnProductsFromUseCase() {
        testCoroutineRule.runBlockingTest {
            val search = "search"
            val result = SearchProductsResult()
            doReturn(result).`when`(productsUseCase).getProductsBy(search)
            viewModel.getProductsBy(search)
            verify(productsUseCase).getProductsBy(search)
            Assert.assertEquals(viewModel.searchResult.value, result)
            Assert.assertEquals(viewModel.loading.value, false)
        }
    }

    @Test
    fun getProductsBySearchShouldShowErrorWhenUseCaseThrowNetworkException() {
        testCoroutineRule.runBlockingTest {
            val search = "search"
            val expected = NetworkException("My error")
            doThrow(expected).`when`(productsUseCase).getProductsBy(search)
            viewModel.getProductsBy(search)
            verify(productsUseCase).getProductsBy(search)
            Assert.assertEquals(viewModel.error.value, expected)
        }
    }
}